/**
 * 
 */
package edu.ucsd.cs110s.temperature;

/**
 * @author jymao
 *
 */
public class Fahrenheit extends Temperature
{
	public Fahrenheit(float t)
	{
		super(t);
	}
	public String toString()
	{
		return "" + this.getValue();
	}
	@Override
	public Temperature toCelsius() {
		return new Celsius((this.getValue() - 32) * 5.0f / 9.0f);
	}
	@Override
	public Temperature toFahrenheit() {
		return this;
	}
}