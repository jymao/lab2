/**
 * 
 */
package edu.ucsd.cs110s.temperature;

/**
 * @author jymao
 *
 */
public class Celsius extends Temperature
{
	public Celsius(float t)
	{
		super(t);
	}
	public String toString()
	{
		return "" + this.getValue();
	}
	@Override
	public Temperature toCelsius() {
		return this;
	}
	@Override
	public Temperature toFahrenheit() {
		return new Fahrenheit((this.getValue() * 9.0f / 5.0f) + 32);
	}
}